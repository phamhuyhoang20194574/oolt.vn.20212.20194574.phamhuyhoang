package hust.soict.globalict.garbage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Garbagecreator {
    public static void main(String[] args) {
    	BufferedReader br = null;
    	
    	try {
			String s ="";
			br = new BufferedReader(new FileReader("src/garbageString.txt"));
			String contentLine = br.readLine();
			while(contentLine != null) {
				s += contentLine;
				contentLine = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
}