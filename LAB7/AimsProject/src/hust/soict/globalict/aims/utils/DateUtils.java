package hust.soict.globalict.aims.utils;

import hust.soict.globalict.aims.utils.MyDate;

public class DateUtils {
    // compare two dates
    public static int compareTwoDates(MyDate d1, MyDate d2) {
        int month1 = Integer.parseInt(d1.getMonth());
        int month2 = Integer.parseInt(d2.getMonth());
        int day1 = Integer.parseInt(d1.getDay());
        int day2 = Integer.parseInt(d2.getDay());
        // if d1>d2 return 1, d1<d2 return -1, d1==d2 return 0
        if (d1.getYear() > d2.getYear()) {
            return 1;
        } else if (d1.getYear() < d2.getYear()) {
            return -1;
        } else {
            if (month1 > month2) {
                return 1;
            } else if (month1 > month2) {
                return -1;
            } else {
                if (day1 > day2) {
                    return 1;
                } else if (day1 < day2) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }

    // sort ASC
    public void sortDates() {
        MyDate[] date = { new MyDate("12", "4", 2022),
                new MyDate("3", "3", 2021),
                new MyDate("30", "12", 2009),
                new MyDate("4", "2", 2012),
                new MyDate("1", "1", 2017)
        };
        MyDate tmp = new MyDate();
        boolean haveSwap = false;
        for (int i = 0; i < date.length - 1; i++) {
            haveSwap = false;
            for (int j = 0; j < date.length - i - 1; j++) {
                if (compareTwoDates(date[j], date[j + 1]) == 1) {
                    tmp = date[j];
                    date[j] = date[j + 1];
                    date[j + 1] = tmp;
                    haveSwap = true;
                }
            }
            if(haveSwap == false){
                break;
            }
        }
        System.out.println("List dates after sort:");
        for(int i = 0; i < date.length; i++){
            System.out.println(date[i].getDay() + "/" + date[i].getMonth() + "/" + date[i].getYear());
        }
    }
}