package hust.soict.globalict.aims.utils;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MyDate {


	    private String day;
	    private String month;
	    private int year;

	    // getter and setter
	    public String getDay() {
	        return day;
	    }

	    public void setDay(String day) {
	        this.day = day;
	    }

	    public String getMonth() {
	        return month;
	    }

	    public void setMonth(String month) {
	        this.month = month;
	    }

	    public int getYear() {
	        return year;
	    }

	    public void setYear(int year) {
	        if (year > 0)
	            this.year = year;
	    }

	    // create constructors
	    public MyDate() {
	        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
	        int current_day = calendar.get(Calendar.DATE);
	        this.day = String.valueOf(current_day) + "th";
	        int current_month = calendar.get(Calendar.MONTH) + 1;
	        this.month = String.valueOf(current_month);
	        this.year = calendar.get(Calendar.YEAR);
	    }

	    // overloading constructor
	    public MyDate(String day, String month, int year) {
	        this.day = day;
	        this.month = month;
	        this.year = year;
	    }

	    public static String getMonthName() {
	        String[] months = { "January", "February",
	                "March", "April", "May", "June", "July",
	                "August", "September", "October", "November",
	                "December" };

	        Calendar calendar = Calendar.getInstance();
	        String month = months[calendar.get(Calendar.MONTH)];
	        return month;
	    }

	    // print current Date with format "February 29th 2020"
	    public void print() {
	        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
	        int current_day = calendar.get(Calendar.DATE);
	        this.day = String.valueOf(current_day) + "th";
	        this.month = getMonthName();
	        this.year = calendar.get(Calendar.YEAR);
	        System.out.println("Current Date: " + this.month + " " + this.day + " " + this.year);
	    }

	    // method which allows users can print a date with format chosen by yourself
	    public void printDate() {
	        int choice;
	        Scanner sc = new Scanner(System.in);
	        Date dateInput = new Date();
	        do {
	            System.out.println("***************MENU***************");
	            System.out.println("1. Print date with format 'yyyy-MM-dd'");
	            System.out.println("2. Print date with format 'd/M/yyyy'");
	            System.out.println("3. Print date with format 'dd-MMM-yyyy'");
	            System.out.println("4. Print date with format 'MMM d yyyy'");
	            System.out.println("5. Print date with format 'mm-dd-yyyy'");
	            System.out.println("6. Exit.");
	            System.out.println("Enter your choice: ");

	            choice = sc.nextInt();

	            switch (choice) {
	                case 1:
	                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
	                    String date1 = simpleDateFormat1.format(dateInput);
	                    System.out.println(date1);
	                    break;
	                case 2:
	                    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("d/M/yyyy");
	                    String date2 = simpleDateFormat2.format(dateInput);
	                    System.out.println(date2);
	                    break;
	                case 3:
	                    SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("dd-MMM-yyyy");
	                    String date3 = simpleDateFormat3.format(dateInput);
	                    System.out.println(date3);
	                    break;
	                case 4:
	                    SimpleDateFormat simpleDateFormat4 = new SimpleDateFormat("MMM d yyyy");
	                    String date4 = simpleDateFormat4.format(dateInput);
	                    System.out.println(date4);
	                    break;
	                case 5:
	                    SimpleDateFormat simpleDateFormat5 = new SimpleDateFormat("mm-dd-yyyy");
	                    String date5 = simpleDateFormat5.format(dateInput);
	                    System.out.println(date5);
	                    break;
	                case 6:
	                    break;
	                default:
	                    System.out.println("Your choice doesn't exist. Please enter the other choice.");
	                    break;
	            }
	        } while (choice != 6);
	        sc.close();
	    }

	}