package hust.soict.globalict.aims.media;

public class Disc extends Media{
     protected String director;
     protected int length;
     public String getDirector() {
    	 return director;
     }
     public int getlength() {
    	 return length;
    	 
     }
     public Disc(String director, int length) {
    	 this.director = director;
    	 this.length = length; 
     }
     public Disc() {
    	 
     }

}
