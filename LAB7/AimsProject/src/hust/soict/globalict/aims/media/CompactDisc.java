package hust.soict.globalict.aims.media;

import java.util.ArrayList;

import java.util.List;
public class CompactDisc extends Disc implements Playable {
       private String artist;
       
       private ArrayList<Track> tracks = new ArrayList<>();
       
       public String getArraylist() {
    	   return artist;
       }
       public int getLength(ArrayList<Track> tracks) {
    	   int sumlength = 0;
    	   for(int i=0 ; i < tracks.size() ; i++) {
    		   sumlength += tracks.get(i).getLength();
    	   
         }
       super.length = sumlength;
       return super.length;
      }
public void addTrack(Track track) {
	if(tracks.contains(track)) {
		System.out.println("Already exist");
	}
	else tracks.add(track);
	
}
public void removeTrack(Track track) {
	if(tracks.contains(track)) {
		tracks.remove(track);
		
	}
	else System.out.println("Does not exist");
}
public void play() {
	for(int i=0 ; i < tracks.size() ; i++) {
    System.out.println("Playing Track: " + tracks.get(i).getTitle());
    System.out.println("Track length: " + tracks.get(i).getLength());
	}
    }
}