package hust.soict.globalict.aims.media;

public abstract class Media {
	protected String title;
    protected String category;
    protected float cost;
	public Media() {
		// TODO Auto-generated constructor stub
	}
	public Media(String title) {
		this.title =title;
	}
	public Media(String title,String category) {
		this(title);
		this.category=category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
    
}
