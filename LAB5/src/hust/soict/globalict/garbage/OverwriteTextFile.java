package hust.soict.globalict.garbage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OverwriteTextFile {
   public static void main(String[] args) {
	   try {
		File myfile = new File("src/garbageString.txt");
		if(myfile.createNewFile()) {
			System.out.println("file created: "+myfile.getName());
		}else {
			System.out.println("file already exists");
		}
	} catch (IOException e) {
		System.out.println("An error");
		e.printStackTrace();
	}
	   
	   try {
		FileWriter myWriter =new FileWriter("src/garbageString.txt");
		
		for(int i=0;i<100000;i++) {
			myWriter.write("making garbage\n");
		}
		myWriter.close();
		System.out.println("Successfully wrote to the file");
		
	} catch (IOException e) {
		System.out.println("Some thing went wrong");
		e.printStackTrace();
	}
   }
}
