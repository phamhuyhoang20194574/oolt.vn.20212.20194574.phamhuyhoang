package hust.soict.globalict.aims;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;
public class Aims{

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			DigitalVideoDisc dvd1 = new DigitalVideoDisc("The lion king");
			dvd1.setCategory("Animation");
			dvd1.setDirector("ROger Allers");
			dvd1.setLength(87);
			dvd1.setCost(19.95f);

			//dvd1.getinfo();
			DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars","Science fiction","george lucas",124, 24.95f);
			DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","animation","john pulic",90,18.99f);
			DigitalVideoDisc dvd4 = new DigitalVideoDisc("Truyen kieu","Truyen","Nguyen Du",200,31.23f);
			dvd1.printInfo();
			dvd2.printInfo();
			dvd3.printInfo();
			dvd4.printInfo();
			//Tao mot mon hang moi
			Order anOrder = new Order();
			
			//Them cac DVD vao don hang
			anOrder.addDigitalVideoDisc(dvd1);
			anOrder.addDigitalVideoDisc(dvd2);
			anOrder.addDigitalVideoDisc(dvd3);
			anOrder.addDigitalVideoDisc(dvd4);
			
			// In ra tong gia tri don hang
			System.out.println("Total cost is: " + anOrder.totalCost());
			anOrder.removeDigitalVideoDisc(dvd4);
			System.out.println("Total cost is: " + anOrder.totalCost());
			anOrder.printOrder();
		}

	}